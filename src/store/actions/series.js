import { GET_LIST_SERIES_BEGIN } from "../../constants/types";

export const getListSeries = () => {
  return {
    type: GET_LIST_SERIES_BEGIN,
  };
};
