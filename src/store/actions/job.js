import { GET_JOB_BEGIN, LOGIN_BEGIN } from "../../constants/types";

export const getJob = () => {
  return {
    type: GET_JOB_BEGIN,
  };
};

export const login = () => {
    return {
      type: LOGIN_BEGIN,
    };
  };

export const searchJob = (keyword) => {
  return {
    type: GET_JOB_BEGIN,
    keyword
  };
};

export const filterJob = (status) => {
  return {
    type: GET_JOB_BEGIN,
    status
  };
};
