import {
  GET_LIST_MOVIES_BEGIN,
  PAGINATION_BEGIN,
  GET_DETAIL_MOVIE_BEGIN,
} from "../../constants/types";

export const getListMovies = () => {
  return {
    type: GET_LIST_MOVIES_BEGIN,
  };
};

export const getDetailMovie = (id) => {
  return {
    type: GET_DETAIL_MOVIE_BEGIN,
    id,
  };
};

export const pagination = (page) => {
  return {
    type: PAGINATION_BEGIN,
    page,
  };
};
