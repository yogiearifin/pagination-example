import {
  GET_JOB_BEGIN,
  GET_JOB_SUCCESS,
  GET_JOB_FAIL,
} from "../../constants/types";

const initialState = {
  jobList: {
    loading: false,
    error: null,
    job: [],
  },
};

export const job = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_JOB_BEGIN:
      return {
        ...state,
        jobList: {
          ...state.jobList,
          loading: true,
          error: null,
        },
      };
    case GET_JOB_SUCCESS:
      return {
        ...state,
        jobList: {
          ...state.jobList,
          loading: false,
          error: null,
          job: payload,
        },
      };
    case GET_JOB_FAIL:
      return {
        ...state,
        jobList: {
          ...state.jobList,
          loading: false,
          error: error,
          job: [],
        },
      };
  }
};
