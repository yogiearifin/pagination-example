import { combineReducers } from "redux";
import { movie } from "./movie";
import { series } from "./series";
import { job } from "./job";

export default combineReducers({ movie, series, job });
