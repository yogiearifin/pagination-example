import {
  GET_LIST_MOVIES_BEGIN,
  GET_LIST_MOVIES_SUCCESS,
  GET_LIST_MOVIES_FAIL,
  PAGINATION_BEGIN,
  PAGINATION_SUCCESS,
  PAGINATION_FAIL,
  GET_DETAIL_MOVIE_BEGIN,
  GET_DETAIL_MOVIE_SUCCESS,
  GET_DETAIL_MOVIE_FAIL,
} from "../../constants/types";

const initialState = {
  listMovie: {
    loading: false,
    error: null,
    list: [],
  },
  detailMovie: {
    loading: false,
    error: null,
    detail: {},
  },
};

export const movie = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_LIST_MOVIES_BEGIN:
      return {
        ...state,
        listMovie: {
          ...state.listMovie,
          loading: true,
          error: null,
        },
      };
    case GET_LIST_MOVIES_SUCCESS:
      return {
        ...state,
        listMovie: {
          ...state.listMovie,
          loading: false,
          error: null,
          list: payload,
        },
      };
    case GET_LIST_MOVIES_FAIL:
      return {
        ...state,
        listMovie: {
          ...state.listMovie,
          loading: false,
          error: error,
          list: [],
        },
      };
    case PAGINATION_BEGIN:
      return {
        ...state,
        listMovie: {
          ...state.listMovie,
          loading: true,
          error: null,
        },
      };
    case PAGINATION_SUCCESS:
      return {
        ...state,
        listMovie: {
          ...state.listMovie,
          loading: false,
          error: null,
          list: payload,
        },
      };
    case PAGINATION_FAIL:
      return {
        ...state,
        listMovie: {
          ...state.listMovie,
          loading: false,
          error: error,
          list: [],
        },
      };
    case GET_DETAIL_MOVIE_BEGIN:
      return {
        ...state,
        detailMovie: {
          ...state.detailMovie,
          loading: true,
          error: null,
        },
      };
    case GET_DETAIL_MOVIE_SUCCESS:
      return {
        ...state,
        detailMovie: {
          ...state.detailMovie,
          loading: false,
          error: null,
          detail: payload,
        },
      };
    case GET_DETAIL_MOVIE_FAIL:
      return {
        ...state,
        detailMovie: {
          ...state.detailMovie,
          loading: false,
          error: error,
          detail: [],
        },
      };
  }
};
