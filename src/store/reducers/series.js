import {
  GET_LIST_SERIES_BEGIN,
  GET_LIST_SERIES_SUCCESS,
  GET_LIST_SERIES_FAIL,
} from "../../constants/types";

const initialState = {
  listSeries: {
    loading: false,
    error: null,
    list: [],
  },
};

export const series = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_LIST_SERIES_BEGIN:
      return {
        ...state,
        listSeries: {
          ...state.listSeries,
          loading: true,
          error: null,
        },
      };
    case GET_LIST_SERIES_SUCCESS:
      return {
        ...state,
        listSeries: {
          ...state.listSeries,
          loading: false,
          error: null,
          list: payload,
        },
      };
    case GET_LIST_SERIES_FAIL:
      return {
        ...state,
        listSeries: {
          ...state.listSeries,
          loading: false,
          error: error,
          list: [],
        },
      };
  }
};
