import { takeEvery, put } from "@redux-saga/core/effects";
import {
  GET_JOB_BEGIN,
  GET_JOB_SUCCESS,
  GET_JOB_FAIL,
  LOGIN_BEGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
} from "../../constants/types";
import axios from "axios";
import { BASE_URL_JOB } from "../../constants/constants";

function* getJob() {
  try {
    const res = yield axios.get(`${BASE_URL_JOB}/pekerjaan`, {
      headers: {
        Authorization:
          "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9taXRyYW1hcy10ZXN0Lmhlcm9rdWFwcC5jb21cL2F1dGhcL2xvZ2luIiwiaWF0IjoxNjM5MTI3NTEyLCJleHAiOjE2MzkxMzExMTIsIm5iZiI6MTYzOTEyNzUxMiwianRpIjoiNHhNdXBQbjlrTTF4QW1CbCIsInN1YiI6MjAsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.c1ylehgRDEqiH6iwz7c6W1a3cNmB1-gbCGPYGh-TNNs",
        "Content-Type": "application/json",
      },
    });
    yield put({
      type: GET_JOB_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_JOB_FAIL,
      error: err,
    });
  }
}

export function* watchGetJob() {
  yield takeEvery(GET_JOB_BEGIN, getJob);
}

function* login() {
  try {
    const res = yield axios.post(`${BASE_URL_JOB}/auth/login`,{
        email:"risyad@mig.id",
        password:"123456"
    });
    yield put({
      type: LOGIN_SUCCESS,
    });
    console.log("res", res);
  } catch (err) {
    yield put({
      type: LOGIN_FAIL,
      error: err,
    });
  }
}

export function* watchLogin() {
  yield takeEvery(LOGIN_BEGIN, login);
}
