import { takeEvery, put } from "@redux-saga/core/effects";
import {
  GET_LIST_SERIES_BEGIN,
  GET_LIST_SERIES_SUCCESS,
  GET_LIST_SERIES_FAIL,
} from "../../constants/types";
import axios from "axios";
import { BASE_URL } from "../../constants/constants";

function* getListSeries() {
  try {
    const res = yield axios.get(
      `${BASE_URL}tv/popular?api_key=${process.env.REACT_APP_API_KEY}`
    );
    yield put({
      type: GET_LIST_SERIES_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_LIST_SERIES_FAIL,
      error: err,
    });
  }
}

export function* watchGetListSeries() {
  yield takeEvery(GET_LIST_SERIES_BEGIN, getListSeries);
}
