import { takeEvery, put } from "@redux-saga/core/effects";
import {
  GET_LIST_MOVIES_BEGIN,
  GET_LIST_MOVIES_SUCCESS,
  GET_LIST_MOVIES_FAIL,
  PAGINATION_BEGIN,
  PAGINATION_SUCCESS,
  PAGINATION_FAIL,
  GET_DETAIL_MOVIE_BEGIN,
  GET_DETAIL_MOVIE_SUCCESS,
  GET_DETAIL_MOVIE_FAIL,
} from "../../constants/types";
import axios from "axios";
import { BASE_URL } from "../../constants/constants";

function* getListMovies() {
  try {
    const res = yield axios.get(
      `${BASE_URL}movie/now_playing?api_key=${process.env.REACT_APP_API_KEY}`
    );
    yield put({
      type: GET_LIST_MOVIES_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_LIST_MOVIES_FAIL,
      error: err,
    });
  }
}

function* pagination(action) {
  const { page } = action;
  try {
    const res = yield axios.get(
      `${BASE_URL}movie/now_playing?api_key=${process.env.REACT_APP_API_KEY}&page=${page}`
    );
    yield put({
      type: PAGINATION_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: PAGINATION_FAIL,
      error: err,
    });
  }
}

function* getDetailMovie(action) {
  const { id } = action;
  try {
    const res = yield axios.get(
      `${BASE_URL}/movie/${id}?api_key=${process.env.REACT_APP_API_KEY}`
    );
    yield put({
      type: GET_DETAIL_MOVIE_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_DETAIL_MOVIE_FAIL,
      error: err,
    });
  }
}

export function* watchGetListMovies() {
  yield takeEvery(GET_LIST_MOVIES_BEGIN, getListMovies);
}

export function* watchPagination() {
  yield takeEvery(PAGINATION_BEGIN, pagination);
}

export function* watchGetDetailMovie() {
  yield takeEvery(GET_DETAIL_MOVIE_BEGIN, getDetailMovie);
}
