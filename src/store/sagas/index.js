import { all } from "redux-saga/effects";
import {
  watchGetListMovies,
  watchPagination,
  watchGetDetailMovie,
} from "./movie";
import { watchGetListSeries } from "./series";
import { watchGetJob,watchLogin } from "./job";

export default function* rootSaga() {
  yield all([
    watchGetListMovies(),
    watchPagination(),
    watchGetListSeries(),
    watchGetDetailMovie(),
    watchGetJob(),
    watchLogin()
  ]);
}
