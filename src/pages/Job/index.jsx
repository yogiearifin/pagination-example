import React,{useEffect} from 'react'
import { getJob,login } from '../../store/actions/job'
import { useDispatch, useSelector } from 'react-redux'

export default function Job() {
    const dispatch = useDispatch()
    useEffect(()=> {
        dispatch(getJob())
        dispatch(login())
    },[dispatch])
    const {loading, job} = useSelector((state)=> state.job.jobList)
    console.log("job", job)
    return (
        <div>
            <h1>Job</h1>
        </div>
    )
}
