import React, { useEffect, useState } from "react";
import { getListMovies, pagination } from "../../store/actions/movies";
import { useDispatch, useSelector } from "react-redux";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import styles from "./assets/home.module.sass";
import CircularProgress from "@mui/material/CircularProgress";
import { BASE_URL_IMAGE } from "../../constants/constants";
import Pagination from "@mui/material/Pagination";
import { getListSeries } from "../../store/actions/series";
import { Link } from "react-router-dom";

export default function Index() {
  const [filter, setFilter] = useState("movie");
  console.log("filter", filter);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListMovies());
  }, []);
  useEffect(() => {
    dispatch(getListSeries());
  }, []);
  const { loading, list } = useSelector((state) => state.movie.listMovie);
  const { loading: loadingSeries, list: listSeries } = useSelector(
    (state) => state.series.listSeries
  );
  console.log("series", listSeries);
  console.log("list", list);
  return (
    <div className={styles.container}>
      <div style={{ display: "flex" }}>
        <h1>This is Home</h1>
        <button onClick={() => setFilter("movie")}>Movie</button>
        <button onClick={() => setFilter("series")}>Series</button>
      </div>
      <div className={styles.containerCard}>
        {loading || loadingSeries ? (
          <CircularProgress />
        ) : filter === "movie" ? (
          list?.results?.map((item, index) => {
            return (
              <Link to={`/movie/${item.id}`} key={index}>
                <div className={styles.cardWrapper}>
                  <Card sx={{ maxWidth: 345, height: "100%" }}>
                    <CardMedia
                      component="img"
                      height="140"
                      image={`${BASE_URL_IMAGE}` + item.backdrop_path}
                      alt={item.title}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="div">
                        {item.title}
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        {item.overview}
                      </Typography>
                    </CardContent>
                  </Card>
                </div>
              </Link>
            );
          })
        ) : (
          listSeries?.results?.map((item, index) => {
            return (
              <div className={styles.cardWrapper} key={index}>
                <Card sx={{ height: "100%", width: 345 }}>
                  <CardMedia
                    component="img"
                    height="140"
                    image={
                      item.backdrop_path
                        ? `${BASE_URL_IMAGE}` + item.backdrop_path
                        : `${BASE_URL_IMAGE}` + item.poster_path
                    }
                    alt={item.name}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {item.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {item.overview
                        ? item.overview
                        : "this series has no overview"}
                    </Typography>
                  </CardContent>
                </Card>
              </div>
            );
          })
        )}
      </div>
      <div className={styles.paginationContainer}>
        <Pagination
          count={list.total_pages}
          variant="outlined"
          shape="rounded"
          onChange={(e) => dispatch(pagination(e.target.textContent))}
          // hideNextButton
          // hidePrevButton
        />
      </div>
    </div>
  );
}
