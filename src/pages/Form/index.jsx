import { useState } from "react";

export default function Form() {
  const [input, setInput] = useState({
    username: "",
    email: "",
    password: "",
    confirm_password: "",
    phone: "",
  });
  const [form, setForm] = useState([
    {
      username: "",
      email: "",
      password: "",
      confirm_password: "",
      phone: "",
    },
  ]);
  console.log("form", form);
  console.log("input", input);
  const changeInput = (e) => {
    setInput({
      ...input,
      [e.target.name]: e.target.value,
    });
  };
  const changeForm = (index, e) => {
    let newForm = [...form];
    newForm[index][e.target.name] = e.target.value;
    setForm(newForm);
  };
  const changePhoneForm = (index, e) => {
    if (/^[0-9]*$/.test(e.target.value)) {
      let newForm = [...form];
      newForm[index].phone = e.target.value;
      setForm(newForm);
    }
  };
  const changePhone = (e) => {
    if (/^[0-9]*$/.test(e.target.value)) {
      setInput({
        ...input,
        phone: e.target.value,
      });
    }
  };

  const isValid = () => {
    if (input.password !== input.confirm_password) {
      alert("password dan confirm password berbeda");
    } else if (input.username === "aini") {
      alert("username mengandung kata tidak senonoh");
    } else {
      alert("berhasil mendaftar");
    }
  };

  const deleteForm = (index) => {
    let newForm = [...form];
    newForm.splice(index, 1);
    setForm(newForm);
  };
  return (
    <div>
      <h1>Form</h1>
      <div>
        {/* <input
          type="text"
          name="username"
          placeholder="username"
          value={input.username}
          onChange={(e) => changeInput(e)}
        />
        <input
          type="text"
          name="email"
          placeholder="email"
          value={input.email}
          onChange={(e) => changeInput(e)}
        />
        <input
          type="password"
          name="password"
          placeholder="password"
          value={input.password}
          onChange={(e) => changeInput(e)}
        />
        <input
          type="password"
          name="confirm_password"
          placeholder="confirm your password"
          value={input.confirm_password}
          onChange={(e) => changeInput(e)}
        />
        <input
          type="text"
          name="phone"
          placeholder="input your phone"
          value={input.phone}
          onChange={(e) => changePhone(e)}
          maxLength={13}
        />
        <button
          disabled={
            !(
              input.username &&
              input.email &&
              input.password &&
              input.confirm_password
            ) &&
            input.password.length < 8 &&
            input.password.length !== input.confirm_password.length &&
            input.phone.length < 9
          }
          onClick={isValid}
        >
          Submit
        </button> */}
        {form.map((item, index) => {
          return (
            <div key={index}>
              <input
                type="text"
                name="username"
                placeholder="username"
                value={item.username || ""}
                onChange={(e) => changeForm(index, e)}
              />
              <input
                type="text"
                name="email"
                placeholder="email"
                value={item.email || ""}
                onChange={(e) => changeForm(index, e)}
              />
              <input
                type="password"
                name="password"
                placeholder="password"
                value={item.password || ""}
                onChange={(e) => changeForm(index, e)}
              />
              <input
                type="password"
                name="confirm_password"
                placeholder="confirm your password"
                value={item.confirm_password || ""}
                onChange={(e) => changeForm(index, e)}
              />
              <input
                type="text"
                name="phone"
                placeholder="input your phone"
                value={item.phone || ""}
                onChange={(e) => changePhoneForm(index, e)}
                maxLength={13}
              />
              <button>Submit</button>
              {index ? (
                <button onClick={() => deleteForm(index)}>Delete</button>
              ) : null}
            </div>
          );
        })}
      </div>
      <div>
        {input.password.length > 0 && input.password.length < 8 ? (
          <p style={{ color: "red", fontWeight: "bold" }}>
            Panjang password minimal 8
          </p>
        ) : null}
        {input.password.length !== input.confirm_password.length ? (
          <p style={{ color: "red", fontWeight: "bold" }}>
            Panjang password dan confirm password tidak sama
          </p>
        ) : null}
        {input.phone.length > 0 && input.phone.length < 9 ? (
          <p style={{ color: "red", fontWeight: "bold" }}>
            Panjang nomor hp minimal 9
          </p>
        ) : null}
      </div>
      <button
        onClick={() =>
          setForm([
            ...form,
            {
              username: "",
              email: "",
              password: "",
              confirm_password: "",
              phone: "",
            },
          ])
        }
      >
        Add Form
      </button>
    </div>
  );
}
