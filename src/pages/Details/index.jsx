import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDetailMovie } from "../../store/actions/movies";
import { useParams } from "react-router";
import { BASE_URL_IMAGE } from "../../constants/constants";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";

const ReadMore = ({ children }) => {
  const text = children;
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };

  return (
    <p className="text">
      {isReadMore ? text?.slice(0, 10) : text}
      <span onClick={toggleReadMore} className="read-or-hide">
        {isReadMore ? "...read more" : " show less"}
      </span>
    </p>
  );
};

export default function Details() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const [image, setImage] = useState();
  const [preview, setPreview] = useState();
  console.log("preview", preview);
  const handleImage = (e) => {
    const file = e.target.files[0];
    setImage(file);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setPreview(reader.result);
    };
    reader.onerror = () => {
      console.log("error on loading image");
    };
  };
  console.log("image", image);
  useEffect(() => {
    dispatch(getDetailMovie(id));
  }, []);
  const { detail, loading } = useSelector((state) => state.movie.detailMovie);
  console.log("detail", detail);
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 24,
  };
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <div>
      {loading ? (
        "loading..."
      ) : (
        <div>
          <h1>{detail.title}</h1>
          <img
            src={BASE_URL_IMAGE + detail.poster_path}
            alt={detail.title}
            onClick={handleOpen}
            style={{ width: "100px" }}
          />
          <ReadMore>{detail.overview}</ReadMore>
          <input
            type="file"
            accept="image/*"
            onChange={(e) => handleImage(e)}
          />
          {preview ? <img src={preview} alt="preview" /> : null}
          <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
          >
            <Box sx={style}>
              <img
                src={BASE_URL_IMAGE + detail.poster_path}
                alt={detail.title}
                onClick={handleOpen}
              />
            </Box>
          </Modal>
        </div>
      )}
    </div>
  );
}
