import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home/index";
import NotFound from "../pages/NotFound/index";
import Details from "../pages/Details/index";
import Form from "../pages/Form/index";
import Job from "../pages/Job/index"

import React from "react";

export default function Routers() {
  return (
    <>
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/movie/:id" exact element={<Details />} />
        <Route path="/form" exact element={<Form />} />
        <Route path="/job" exact element={<Job />} />
      </Routes>
    </>
  );
}
